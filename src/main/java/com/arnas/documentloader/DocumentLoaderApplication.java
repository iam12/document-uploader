package com.arnas.documentloader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DocumentLoaderApplication {

	public static void main(String[] args) {
		SpringApplication.run(DocumentLoaderApplication.class, args);
	}

}
